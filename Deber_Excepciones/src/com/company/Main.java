package com.company;

public class Main {

    @SuppressWarnings("ConstantConditions")
    public static void main(String[] args) {
        // write your code here

        Object obj = new Object();
        Fruit objFruit = new Fruit();
        Apple objApple = new Apple();
        Citrus objCitrus = new Citrus();
        Orange objOreange = new Orange();
        Squeezable squeezable = new Squeezable() {
        };

        try {
            objFruit = (Fruit) obj;
        } catch (ClassCastException c) {
            System.out.println(c);
        }
        try {
            objFruit = (Fruit) objApple;
        } catch (ClassCastException c) {
            System.out.println(c);
        }

        try {
            objFruit = (Fruit) objCitrus;
        } catch (ClassCastException c) {
            System.out.println(c);

        }
        try {
            objFruit = (Fruit) objOreange;
        } catch (ClassCastException c) {
            System.out.println(c);
        }

        try {
            objFruit = (Fruit) squeezable;
        } catch (ClassCastException c) {
            System.out.println(c);
        }


        try {
            objApple = (Apple) obj;
        } catch (ClassCastException c) {
            System.out.println(c);
        }
        try {
            objApple = (Apple) objFruit;
        } catch (ClassCastException c) {
            System.out.println(c);
        }


        try {
            squeezable = (Squeezable) objFruit;
        } catch (ClassCastException c) {
            System.out.println(c);
        }
        try {
            squeezable = (Squeezable) objApple;
        } catch (ClassCastException c) {
            System.out.println(c);
        }

        try {
            squeezable = (Squeezable) objCitrus;
        } catch (ClassCastException c) {
            System.out.println(c);
        }

        try {
            squeezable = (Squeezable) obj;
        } catch (ClassCastException c) {
            System.out.println(c);
        }
         try {
          squeezable = (Squeezable) objOreange;
         }catch (ClassCastException c){
             System.out.println(c);
         }

            try {
                objCitrus = (Citrus) obj;
            } catch (ClassCastException c) {
                System.out.println(c);
            }

            try {
                objCitrus = (Citrus) objFruit;
            } catch (ClassCastException c) {
                System.out.println(c);
            }
            try {
                objCitrus = (Citrus) objOreange;
            } catch (ClassCastException c) {
                System.out.println(c);
            }

            try {
                objCitrus = (Citrus) squeezable;
            } catch (ClassCastException c) {
                System.out.println(c);
            }

    try{
            objOreange = (Orange) objCitrus;
    }catch (ClassCastException c){
        System.out.println(c);
    }
    try{
            objOreange = (Orange) objFruit;
    }catch (ClassCastException c){
        System.out.println(c);
    }

    try{
            objOreange = (Orange) obj;
    }catch (ClassCastException c){
        System.out.println(c);
    }
    try{
            objOreange = (Orange) squeezable;
    }catch (ClassCastException c){
        System.out.println(c);
    }


        }
}





